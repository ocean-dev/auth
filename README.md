# The Auth Service

This is the Auth Service for Ocean. It performs all authentication and
authorisation.


## Installation

First start the common infrastructure, if it isn't already running:
```
  docker-compose -f ../ocean/common.yml up -d --build
```
Then, to run only the Auth service:
```
  docker-compose -f ../ocean/services.yml up --build auth_service
```
To run the service setup/update rake task:
```
  docker-compose -f ../ocean/services.yml run auth_service rake ocean:setup_all
```

The `ocean:setup_all` task executes the following rake tasks in order:
```
  db:create
  db:migrate
  ocean:create_seeding_data
  ocean:update_api_users
  ocean:update_services_resources_and_rights
  ocean:update_roles
  ocean:update_groups
  ocean:update_god
```
They are all idempotent and can be run at any time. You will want to run
`ocean:setup_all` when any of the above data has changed. On AWS this is handled
as part of the setup procedure, but in the local version you must do it
yourself.

## Running the RSpecs

If you intend to run RSpec tests, start the common infrastructure, if it isn't
already running:
```
  docker-compose -f ../ocean/common.yml up -d --build
```
Next create and migrate the test database:
```
  docker-compose -f ../ocean/services.yml run --rm -e RAILS_ENV=test auth_service rake db:create
  docker-compose -f ../ocean/services.yml run --rm -e RAILS_ENV=test auth_service rake db:migrate
```
The source is accessible through a Docker volume, so any changes you make to the
source will be propagated to the container. You can now build the Docker image:
```
  docker-compose -f ../ocean/services.yml build auth_service
```
If you modify gems, simply run `bundler update` locally, then rebuild the image.

To run the specs:
```
  docker-compose -f ../ocean/services.yml run --rm -e LOG_LEVEL=fatal auth_service rspec
```
RSpec has been set up to save any failed examples so you can use
`rspec --next-failure` and other switches on subsequent runs. If no failed
examples exist, the whole suite will be run.
