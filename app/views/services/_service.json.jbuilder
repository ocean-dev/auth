json.service do |json|
  json.call(service, :name, :description, :lock_version)
  json.created_at service.created_at.utc.iso8601
  json.updated_at service.updated_at.utc.iso8601
  json._links hyperlinks(
    self:           service_url(service),
    documentation: service.documentation_href.present? &&
                   { href: service.documentation_href,
                     type: 'text/html' },
    resources:     resources_service_url(service),
    instances:     instances_url(ocean_env: OCEAN_ENV, service: service.name),
    creator:       smart_api_user_url(service.created_by),
    updater:       smart_api_user_url(service.updated_by)
  )
end
