json.resource do |json|
  json.call(resource, :name, :description, :lock_version)
  json.created_at resource.created_at.utc.iso8601
  json.updated_at resource.updated_at.utc.iso8601
  json._links hyperlinks(
    self:          resource_url(resource),
    documentation: resource.documentation_href.present? &&
                   { href: resource.documentation_href,
                     type: 'text/html' },
    service:       service_url(resource.service),
    rights:        rights_resource_url(resource),
    creator:   smart_api_user_url(resource.created_by),
    updater:   smart_api_user_url(resource.updated_by)
  )
end
