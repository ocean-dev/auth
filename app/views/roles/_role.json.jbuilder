json.role do |json|
  json.call(role, :name, :description, :lock_version)
  json.created_at     role.created_at.utc.iso8601
  json.updated_at     role.updated_at.utc.iso8601
  json.indestructible role.indestructible if role.indestructible
  json._links hyperlinks(
    self:      role_url(role),
    documentation: role.documentation_href.present? &&
                     { href: role.documentation_href,
                       type: 'text/html' },
    api_users: api_users_role_url(role),
    groups:    groups_role_url(role),
    rights:    rights_role_url(role),
    connect:   connect_role_url(role),
    creator:   smart_api_user_url(role.created_by),
    updater:   smart_api_user_url(role.updated_by)
  )
end
