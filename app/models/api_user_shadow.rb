class ApiUserShadow < OceanDynamo::Table
  ocean_resource_model index: [:username],
                       search: false,
                       invalidate_member: [],
                       invalidate_collection: []

  dynamo_schema(:username, table_name_suffix: Api.basename_suffix,
                           timestamps: nil, create: true, locking: false,
                           client: $dynamo_client) do
    attribute :api_user_id,              :string
    attribute :password_hash,            :string
    attribute :password_salt,            :string
    attribute :authentication_duration,  :integer
    attribute :login_blocked,            :boolean, default: false
    attribute :login_blocked_reason,     :string
  end

  validates :username, presence: true
  validates :api_user_id, presence: true

  def self.find_by_credentials(un, pw)
    # Don't bother going to the DB if credentials are missing
    return nil if un == '' && pw == ''
    # Consult the DB
    user = find_by_key(un) || false
    user && !!user.authenticates?(pw) && user
  end

  def authenticates?(plaintext_password)
    password_hash == BCrypt::Engine.hash_secret(plaintext_password, password_salt)
  end

  def latest_authentication
    Authentication._late_connect?
    options = Authentication.condition_builder(Authentication.table_hash_key, username,
                                               Authentication.table_range_key, '>=', 0,
                                               scan_index_forward: false, limit: 1,
                                               consistent: true)
    Authentication.in_batches :query, options do |attrs|
      return Authentication.new._setup_from_dynamo(attrs)
    end
    nil
  end
end
