# == Schema Information
#
# Table name: groups
#
#  id                 :string           primary key
#  name               :string           not null
#  description        :string           default(""), not null
#  lock_version       :integer          default(0), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  created_by         :string
#  updated_by         :string
#  indestructible     :boolean          default(FALSE), not null
#  documentation_href :string
#
# Indexes
#
#  index_groups_on_created_by  (created_by)
#  index_groups_on_id          (id) UNIQUE
#  index_groups_on_name        (name) UNIQUE
#  index_groups_on_updated_at  (updated_at)
#  index_groups_on_updated_by  (updated_by)
#

class Group < ActiveRecord::Base
  self.primary_key = 'id'

  def initialize(*)
    super
    self.id ||= SecureRandom.uuid
  end

  ocean_resource_model

  # Relations
  has_and_belongs_to_many :api_users,         # via api_users_groups
                          after_add:    %i[touch_both invalidate_user_authorizations],
                          after_remove: %i[touch_both invalidate_user_authorizations]

  has_and_belongs_to_many :roles,             # via groups_roles
                          after_add:    :touch_both,
                          after_remove: %i[touch_both invalidate_role_rights]

  has_and_belongs_to_many :rights,            # via groups_rights
                          after_add:    :touch_both,
                          after_remove: %i[touch_both invalidate_right_authorizations]

  def invalidate_user_authorizations(api_user)
    api_user.authentications.each { |auth| Api.ban auth.token }
  end

  def invalidate_right_authorizations(right)
    Api.ban right.name
  end

  def invalidate_role_rights(role)
    role.invalidate_role_rights
  end

  # Attributes
  attr_accessible :description, :lock_version, :name, :documentation_href

  # Validations
  validates :name, presence: true
end
