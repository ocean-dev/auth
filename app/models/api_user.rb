# == Schema Information
#
# Table name: api_users
#
#  id                      :string           primary key
#  username                :string           not null
#  password_hash           :string           not null
#  password_salt           :string           not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  real_name               :string           default("")
#  lock_version            :integer          default(0), not null
#  email                   :string           default(""), not null
#  created_by              :string
#  updated_by              :string
#  authentication_duration :integer          default(10800), not null
#  login_blocked           :boolean          default(FALSE), not null
#  login_blocked_reason    :string
#  indestructible          :boolean          default(FALSE), not null
#
# Indexes
#
#  index_api_users_on_created_by  (created_by)
#  index_api_users_on_id          (id) UNIQUE
#  index_api_users_on_updated_at  (updated_at)
#  index_api_users_on_updated_by  (updated_by)
#  index_api_users_on_username    (username) UNIQUE
#

class ApiUser < ActiveRecord::Base
  self.primary_key = 'id'


  def initialize(*)
    super
    self.id ||= SecureRandom.uuid
  end

  ocean_resource_model index: %i[username real_name email],
                       search: :email

  # Relations

  def authentications(t = 0)
    Authentication._late_connect?
    result = []
    options = Authentication.condition_builder(Authentication.table_hash_key, username,
                                               Authentication.table_range_key, '>=', t.to_i,
                                               consistent: true)
    Authentication.in_batches :query, options do |attrs|
      result << Authentication.new._setup_from_dynamo(attrs)
    end
    result
  end

  has_and_belongs_to_many :groups,    # via api_users_groups
                          after_add:    %i[touch_both invalidate_user_authorizations],
                          after_remove: %i[touch_both invalidate_user_authorizations]

  has_and_belongs_to_many :roles,     # via api_users_roles
                          after_add:    [:touch_both],
                          after_remove: %i[touch_both invalidate_role_rights]

  def invalidate_user_authorizations(_other)
    authentications.each { |auth| Api.ban auth.token }
  end

  def invalidate_role_rights(role)
    role.invalidate_role_rights
  end

  # Attributes
  attr_accessible :username, :password, :real_name, :email, :lock_version,
                  :authentication_duration
  attr_accessor :password

  # Validations
  validates :username, presence: true
  validates :username, format: /\A[A-Za-z0-9@._-]{2,}\z/, allow_blank: true
  validates :lock_version, presence: true
  validates :email, presence: true
  validates :email, email: { message: 'is an invalid email address' }, allow_blank: true
  validates :authentication_duration, presence: true,
                                      numericality: { only_integer: true, greater_than: 0 }
  validates :password, presence: true, on: :create, if: ->(u) { u.password_hash.blank? }
  validate :validate_password_and_set_salt_and_hash
  # validates :password_hash, presence: true, unless: lambda { |u| u.errors[:password].present? }
  # validates :password_salt, presence: true, unless: lambda { |u| u.errors[:password].present? }

  def validate_password_and_set_salt_and_hash
    return if password.blank?
    # Check
    if PASSWORD_REGEXP.present? && Regexp.new(PASSWORD_REGEXP) !~ password
      errors.add :password, PASSWORD_MSG
      return
    end
    # Set the salt and the hashed password
    self.password_salt = BCrypt::Engine.generate_salt
    self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
  end

  # Callbacks
  around_save do |api_user, block|
    delete_old = api_user.username_changed? && api_user.username_was
    old_username = api_user.username_was
    block.call
    ApiUserShadow.find(old_username).destroy if delete_old
    ApiUserShadow.create username: api_user.username,
                         api_user_id: api_user.id,
                         password_hash: api_user.password_hash,
                         password_salt: api_user.password_salt,
                         authentication_duration: api_user.authentication_duration,
                         login_blocked: api_user.login_blocked,
                         login_blocked_reason: api_user.login_blocked_reason
  end

  after_destroy do |api_user|
    api_user_shadow.destroy
    api_user.authentications.each(&:destroy)
  end

  def self.find_by_credentials(un, pw)
    # Don't bother going to the DB if credentials are missing
    return nil if un == '' && pw == ''
    # Consult the DB
    user = find_by_username(un)
    user && !!user.authenticates?(pw) && user
  end

  def authenticates?(plaintext_password)
    password_hash == BCrypt::Engine.hash_secret(plaintext_password, password_salt)
  end

  #
  # Map each right of this ApiUser to a function. Stop if fn returns true for
  # a matching right. This method returns either the matching right, if one
  # was found, or false if none was found or if fn filtered away all matches.
  #
  # Each right is only considered once. The function is mandatory; each keyword
  # adds further restriction if present and non-false.
  #
  def map_rights(fn, service: nil, resource: nil, hyperlink: nil, verb: nil,
                 app: nil, context: nil, app_context_acc_fn: nil)
    seen_rights = []
    seen_roles = []
    rsrc = Resource.where(name: resource).first if resource
    cond = rsrc ? { resource_id: rsrc.id } : {}
    # Local function to consider a right. Since this is a Proc (not a lambda), any
    # return in the body will return from the enclosing function (map_rights) rather
    # than from the Proc. This is exactly what we want.
    considerer = proc do |right|
      unless seen_rights.include?(right)
        seen_rights << right
        if (!service   ||                      right.service.name == service) &&
           (!resource  ||                      right.resource.name == resource) &&
           (!hyperlink || right.hyperlink == '*' || right.hyperlink == hyperlink) &&
           (!verb      || right.verb == '*'      || right.verb == verb)
          # Only the app and context might differ. Process them if function given.
          app_context_acc_fn.call(right) if app_context_acc_fn
          # Now check if this is a full match. If so, call fn.
          if (!app       || right.app == '*'       || right.app == app) &&
             (!context   || right.context == '*'   || right.context == context)
            # If the right authorises the request, return the right, unless a
            # false value returned from fn forces the search to continue.
            return right if fn.call(right)
          end
        end
      end
    end
    roles.each do |role|
      seen_roles << role
      role.rights.where(cond).each { |right| considerer.call(right) }
    end
    groups.each do |group|
      group.rights.where(cond).each { |right| considerer.call(right) }
      group.roles.each do |role|
        next if seen_roles.include?(role)
        seen_roles << role
        role.rights.where(cond).each { |right| considerer.call(right) }
      end
    end
    false
  end

  def effective_rights
    result = []
    map_rights(->(right) { result << right; false })
    result
  end

  def api_user_shadow
    @api_user_shadow ||= ApiUserShadow.find(username, consistent: true)
  end
end
