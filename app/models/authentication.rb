class Authentication < OceanDynamo::Table
  ocean_resource_model index: [:token],
                       search: false,
                       invalidate_member: [],
                       invalidate_collection: []

  dynamo_schema(:username, :expires_at,
                table_name_suffix: Api.basename_suffix,
                timestamps: nil,
                create: true,
                locking: false,
                client: $dynamo_client) do
    attribute :token,       :string
    attribute :max_age,     :integer
    attribute :created_at,  :datetime
    attribute :expires_at,  :datetime
    attribute :api_user_id, :string
    attribute :const,       :string, default: 'c'

    global_secondary_index :token, :created_at, projection: :all
    global_secondary_index :const, :expires_at
  end

  # Relations

  # belongs_to :api_user
  def api_user=(u)
    self.api_user_id = u.id
  end

  def api_user
    ApiUser.find api_user_id
  end

  # Callbacks
  after_destroy do |auth|
    # The following line invalidates all authorisations done using this Authentication
    Api.ban auth.token
    true
  end

  # Class methods

  def self.new_token
    SecureRandom.urlsafe_base64(32)
  end

  # Instance methods

  def seconds_remaining
    sec = (expires_at.utc - Time.now.utc).to_i
    sec = 0 if sec < 0
    sec
  end

  def expired?
    seconds_remaining <= 0
  end

  def active?
    !expired?
  end

  def authorized?(service, resource, hyperlink, verb, app, context)
    result = false
    acs = []
    wildcarded = (app == '*' && context == '*')
    # Examine all rights, don't stop at a full match if both app and context are wildcarded
    api_user.map_rights(->(right) { result = right; !wildcarded },
                        app_context_acc_fn: ->(right) { acs << { 'app' => right.app, 'context' => right.context } unless right.app == '*' && right.context == '*' },
                        service: service, resource: resource,
                        hyperlink: hyperlink, verb: verb,
                        app: app, context: context)
    return result if result
    wildcarded && acs != [] && acs
  end

  def self.cleanup
    t = (Time.now - EXPIRED_AUTHENTICATION_LIFE).utc
    killed = 0
    Authentication.find_global_each(:const, 'c', :expires_at, '<=', t) do |auth|
      auth.destroy
      killed += 1
    end
    killed
  end
end
