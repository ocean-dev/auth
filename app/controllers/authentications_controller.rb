class AuthenticationsController < ApplicationController
  ocean_resource_controller required_attributes: [],
                            extra_actions: { 'cleanup' => %w[cleanup PUT] }

  skip_before_action :require_x_api_token, only: %i[create show]
  skip_before_action :authorize_action,    only: %i[create show]

  before_action :find_authentication,      only: %i[show destroy]
  before_action :find_api_user,            only: :create
  before_action :ensure_not_blocked,       only: :create

  #
  # POST /authentications
  #
  # This action is used for authenticating an ApiUser.
  #
  def create
    token = nil
    @authentication = @api_user.latest_authentication  # Find the latest auth for the user
    if @authentication && @authentication.expired?     # Got an expired one?
      token = @authentication.token                    # Re-use its token
      @authentication = false                          # But don't re-use the auth itself
    end
    unless @authentication
      max_age = @api_user.authentication_duration
      @authentication = Authentication.create!(api_user_id: @api_user.api_user_id,
                                               username: @api_user.username,
                                               token: token || Authentication.new_token,
                                               max_age: max_age,
                                               created_at: Time.now.utc,
                                               expires_at: Time.now.utc + max_age)
    end
    Thread.current[:username] = @api_user.username
    Thread.current[:x_api_token] = @authentication.token
    expires_now # Tiny increase in security
    @right = nil
    @group_names = nil
    render partial: 'authentication', object: @authentication, status: 201
  end

  #
  # GET /authentications/<token>?query=<query>
  #
  # This action performs authorisation based on the token of a previous
  # Authentication.
  #
  def show
    username = @authentication.username
    token = @authentication.token
    Thread.current[:username] = username
    Thread.current[:x_api_token] = token
    # Has the authentication expired?
    if @authentication.expired?
      logger.info "Authentication EXPIRED for #{username}"
      expires_now # We're re-using authentications
      render_api_error 419, 'Authentication Timeout'
      return
    end
    # Is the query string present?
    query = params[:query]
    if query.blank?
      expires_in 0, 's-maxage' => 1.day, 'max-stale' => 0, public: true
      render_api_error 422, 'Query missing'
      return
    end
    # Is the query string well-formed?
    query = query.to_s.split(':')
    if query.length != 6
      expires_in 0, 's-maxage' => 1.day, 'max-stale' => 0, public: true
      render_api_error 422, 'Malformed query string',
                       'Must consist of exactly six colon-separated parts'
      return
    end
    # Is the verb a supported one?
    unless ['*', 'POST', 'GET', 'GET*', 'PUT', 'DELETE', 'DELETE*'].include?(query[3])
      expires_in 0, 's-maxage' => 1.day, 'max-stale' => 0, public: true
      render_api_error 422, 'Malformed query string',
                       'Unsupported verb'
      return
    end
    # Is the client authorised to perform the query?
    @right = @authentication.authorized?(*query)
    unless @right
      msg = "Authorization DENIED: #{username} may NOT <#{params[:query]}>"
      logger.warn msg
      expires_in 0, 's-maxage' => 5.seconds, 'max-stale' => 0, public: true
      render_api_error 403, 'Denied'
      return
    end
    # Let the authorisation live in the Varnish cache while it's valid
    return unless stale?(last_modified: @authentication.created_at, etag: @authentication)
    expires_in 0, 's-maxage' => AUTHORIZATION_DURATION, 'max-stale' => 0, public: true
    api_render @authentication, override_partial: 'authentications/authentication'
  end

  #
  # DELETE /authentications/<token>
  #
  # This action revokes an authentication and all its authorisations.
  #
  def destroy
    @authentication.destroy
    render_head_204
  end

  #
  # PUT /authentications/cleanup
  #
  def cleanup
    killed = Authentication.cleanup
    logger.info "Cleaned up #{killed} old expired Authentications."
    render json: { cleaned_up: killed }
  end

  private

  def find_authentication
    @authentication = Authentication.find_global(:token, params[:id],
                                                 :created_at, '>=', 0,
                                                 scan_index_forward: false,
                                                 limit: 1).first
    if @authentication
      Thread.current[:username] = @authentication.api_user.username
      Thread.current[:token] = @authentication.token
      return true
    end
    msg = "Authentication not found for [#{params[:id]}]"
    logger.info msg
    expires_in 0, 's-maxage' => 1.day, 'max-stale' => 0
    render_api_error 400, 'Authentication not found'
    false
  end

  def find_api_user
    username, password = Api.decode_credentials(request.headers['X-API-Authenticate'])
    if username == '' && password == ''
      msg = "Authentication failed with malformed credentials for '#{username}'"
      logger.warn msg
      expires_in 0, 's-maxage' => 1.day, 'max-stale' => 0
      render_api_error 400, 'Malformed credentials'
      return false
    end
    @api_user = ApiUserShadow.find_by_credentials(username, password)
    unless @api_user
      msg = "Authentication doesn't authenticate for '#{username}'"
      logger.warn msg
      expires_in 0, 's-maxage' => 10.seconds, 'max-stale' => 0
      render_api_error 403, 'Does not authenticate'
      return false
    end
    true
  end

  def ensure_not_blocked
    return true unless @api_user.login_blocked
    msg = "Authentication blocked for '#{@api_user.username}': \"#{@api_user.login_blocked_reason}\""
    logger.warn msg
    expires_in 0, 's-maxage' => 10.seconds, 'max-stale' => 0
    res = ['Login blocked']
    res << @api_user.login_blocked_reason if @api_user.login_blocked_reason.present?
    render_api_error 403, *res
    false
  end
end
