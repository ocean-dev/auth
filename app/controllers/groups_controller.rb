class GroupsController < ApplicationController
  ocean_resource_controller extra_actions: { 'api_users' => %w[api_users GET],
                                             'roles'     => %w[roles GET],
                                             'rights'    => %w[rights GET] }

  before_action :find_group, except: %i[index create]
  before_action :find_connectee, only: %i[connect disconnect]

  # GET /groups
  def index
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    return unless stale?(collection_etag(Group))
    @groups = Group.collection(params)
    api_render @groups
  end

  # GET /groups/1
  def show
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    api_render @group if stale?(@group)
  end

  # POST /groups
  def create
    @group = Group.new(filtered_params(Group))
    set_updater(@group)
    @group.save!
    api_render @group, new: true
  end

  # PUT /groups/1
  def update
    if missing_attributes?
      render_api_error 422, 'Missing resource attributes'
      return
    end
    @group.assign_attributes(filtered_params(Group))
    set_updater(@group)
    @group.save!
    api_render @group
  end

  # DELETE /groups/1
  def destroy
    render_api_error(403, 'Indestructible') && return if @group.indestructible
    @group.destroy
    render_head_204
  end

  # GET /groups/1/api_users
  def api_users
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    api_render @group.api_users if stale?(collection_etag(@group.api_users))
  end

  # GET /groups/1/roles
  def roles
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    api_render @group.roles if stale?(collection_etag(@group.roles))
  end

  # GET /groups/1/rights
  def rights
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    api_render @group.rights if stale?(collection_etag(@group.rights))
  end

  # PUT /groups/1/connect
  def connect
    begin
      case @connectee_class.to_s
      when 'ApiUser' then @group.api_users << @connectee
      when 'Role'    then @group.roles << @connectee
      when 'Right'   then @group.rights << @connectee
      else
        render_api_error 422, 'Unsupported connection'
        return
      end
    rescue ActiveRecord::RecordNotUnique, ActiveRecord::StatementInvalid, SQLite3::ConstraintException
      # The connectee is already connected: do nothing.
    end
    @group.touch
    @connectee.touch
    render_head_204
  end

  # DELETE /groups/1/connect
  def disconnect
    case @connectee_class.to_s
    when 'ApiUser' then @group.api_users.delete(@connectee)
    when 'Role'    then @group.roles.delete(@connectee)
    when 'Right'   then @group.rights.delete(@connectee)
    else
      render_api_error 422, 'Unsupported connection'
      return
    end
    @group.touch
    @connectee.touch
    render_head_204
  end

  private

  def find_group
    @group = Group.find_by_id params[:id]
    return true if @group
    render_api_error 404, 'Group not found'
    false
  end
end
