module ApplicationHelper
  def instances_url(ocean_env: OCEAN_ENV, service: null)
    "#{OCEAN_API_URL}/#{Api.version_for :instances}/instances?ocean_env=#{ocean_env}&service=#{service}"
  end
end
