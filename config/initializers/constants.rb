
DEFAULT_CACHE_TIME = 1.week

AUTHORIZATION_DURATION = 3.hours

EXPIRED_AUTHENTICATION_LIFE = 1.day
