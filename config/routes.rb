# == Route Map
#
#                   Prefix Verb   URI Pattern                                 Controller#Action
#        resources_service GET    /v1/services/:id/resources(.:format)        services#resources {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                 services GET    /v1/services(.:format)                      services#index
#                  service GET    /v1/services/:id(.:format)                  services#show {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#          rights_resource GET    /v1/resources/:id/rights(.:format)          resources#rights {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          POST   /v1/resources/:id/rights(.:format)          resources#right_create {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                resources GET    /v1/resources(.:format)                     resources#index
#                 resource GET    /v1/resources/:id(.:format)                 resources#show {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#             groups_right GET    /v1/rights/:id/groups(.:format)             rights#groups {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#              roles_right GET    /v1/rights/:id/roles(.:format)              rights#roles {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#            connect_right PUT    /v1/rights/:id/connect(.:format)            rights#connect {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          DELETE /v1/rights/:id/connect(.:format)            rights#disconnect {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                   rights GET    /v1/rights(.:format)                        rights#index
#                    right GET    /v1/rights/:id(.:format)                    rights#show {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          PATCH  /v1/rights/:id(.:format)                    rights#update {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          PUT    /v1/rights/:id(.:format)                    rights#update {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          DELETE /v1/rights/:id(.:format)                    rights#destroy {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#           api_users_role GET    /v1/roles/:id/api_users(.:format)           roles#api_users {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#              groups_role GET    /v1/roles/:id/groups(.:format)              roles#groups {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#              rights_role GET    /v1/roles/:id/rights(.:format)              roles#rights {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#             connect_role PUT    /v1/roles/:id/connect(.:format)             roles#connect {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          DELETE /v1/roles/:id/connect(.:format)             roles#disconnect {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                    roles GET    /v1/roles(.:format)                         roles#index
#                          POST   /v1/roles(.:format)                         roles#create
#                     role GET    /v1/roles/:id(.:format)                     roles#show {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          PATCH  /v1/roles/:id(.:format)                     roles#update {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          PUT    /v1/roles/:id(.:format)                     roles#update {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          DELETE /v1/roles/:id(.:format)                     roles#destroy {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#          api_users_group GET    /v1/groups/:id/api_users(.:format)          groups#api_users {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#              roles_group GET    /v1/groups/:id/roles(.:format)              groups#roles {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#             rights_group GET    /v1/groups/:id/rights(.:format)             groups#rights {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#            connect_group PUT    /v1/groups/:id/connect(.:format)            groups#connect {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          DELETE /v1/groups/:id/connect(.:format)            groups#disconnect {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                   groups GET    /v1/groups(.:format)                        groups#index
#                          POST   /v1/groups(.:format)                        groups#create
#                    group GET    /v1/groups/:id(.:format)                    groups#show {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          PATCH  /v1/groups/:id(.:format)                    groups#update {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          PUT    /v1/groups/:id(.:format)                    groups#update {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          DELETE /v1/groups/:id(.:format)                    groups#destroy {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
# authentications_api_user GET    /v1/api_users/:id/authentications(.:format) api_users#authentications {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#           roles_api_user GET    /v1/api_users/:id/roles(.:format)           api_users#roles {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#          groups_api_user GET    /v1/api_users/:id/groups(.:format)          api_users#groups {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#          rights_api_user GET    /v1/api_users/:id/rights(.:format)          api_users#rights {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#         connect_api_user PUT    /v1/api_users/:id/connect(.:format)         api_users#connect {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          DELETE /v1/api_users/:id/connect(.:format)         api_users#disconnect {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                api_users GET    /v1/api_users(.:format)                     api_users#index
#                          POST   /v1/api_users(.:format)                     api_users#create
#                 api_user GET    /v1/api_users/:id(.:format)                 api_users#show {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          PATCH  /v1/api_users/:id(.:format)                 api_users#update {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          PUT    /v1/api_users/:id(.:format)                 api_users#update {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                          DELETE /v1/api_users/:id(.:format)                 api_users#destroy {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#  cleanup_authentications PUT    /v1/authentications/cleanup(.:format)       authentications#cleanup
#          authentications POST   /v1/authentications(.:format)               authentications#create
#           authentication GET    /v1/authentications/:id(.:format)           authentications#show {:id=>/.+/}
#                          DELETE /v1/authentications/:id(.:format)           authentications#destroy {:id=>/.+/}
#                    alive GET    /alive(.:format)                            alive#index
#

Auth::Application.routes.draw do
  scope '/v1' do
    resources :services, except: %i[create new edit update destroy],
                         constraints: { id: UUID_REGEX } do
      member do
        get  'resources'
      end
    end

    resources :resources, except: %i[create new edit update destroy],
                          constraints: { id: UUID_REGEX } do
      member do
        get  'rights'
        post 'rights' => 'resources#right_create'
      end
    end

    resources :rights, except: %i[create new edit],
                       constraints: { id: UUID_REGEX } do
      member do
        get 'groups'
        get 'roles'
        put    'connect'
        delete 'connect' => 'rights#disconnect'
      end
    end

    resources :roles, except: %i[new edit],
                      constraints: { id: UUID_REGEX } do
      member do
        get 'api_users'
        get 'groups'
        get 'rights'
        put    'connect'
        delete 'connect' => 'roles#disconnect'
      end
    end

    resources :groups, except: %i[new edit],
                       constraints: { id: UUID_REGEX } do
      member do
        get 'api_users'
        get 'roles'
        get 'rights'
        put    'connect'
        delete 'connect' => 'groups#disconnect'
      end
    end

    resources :api_users, except: %i[new edit],
                          constraints: { id: UUID_REGEX } do
      member do
        get 'authentications'
        get 'roles'
        get 'groups'
        get 'rights'
        put    'connect'
        delete 'connect' => 'api_users#disconnect'
      end
    end

    resources :authentications, except: %i[index new edit update],
                                constraints: { id: /.+/ } do
      collection do
        put 'cleanup'
      end
    end
  end
end
