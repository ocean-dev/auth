name: auth
description: Authentication and authorisation
documentation_href: http://wiki.oceanframework.net/Auth_-_Authentication_and_Authorisation

resources:

- name: services
  description: The Service resource describes a service available in the SOA and
    the Resources it handles.
  documentation_href: http://wiki.oceanframework.net/The_Service_Resource
  version: v1
  rights:
  - description: Service resource God
  - description: Get a Service
    hyperlink: self
    verb: GET
  - description: Get a collection of Services
    hyperlink: self
    verb: GET*
  - description: Get a collection of the Service's Resources
    hyperlink: resources
    verb: GET

- name: resources
  description: The Resource resource describes a Resource belonging to a Service,
    as well as the Rights it implements.
  documentation_href: http://wiki.oceanframework.net/The_Resource_Resource
  version: v1
  rights:
  - description: Resource resource God
  - description: Get a Resource
    hyperlink: self
    verb: GET
  - description: Get a collection of Resources
    hyperlink: self
    verb: GET*
  - description: Get a collection of the Resource's Rights
    hyperlink: rights
    verb: GET
  - description: Create a new Right for this Resource
    hyperlink: rights
    verb: POST

- name: rights
  description: The Right resource describes an access right used for authorisation
    of a REST operation. Each Right belongs to a Resource.
  documentation_href: http://wiki.oceanframework.net/The_Right_Resource
  version: v1
  rights:
  - description: Right resource God
  - description: Get a Right
    hyperlink: self
    verb: GET
  - description: Modify a Right
    hyperlink: self
    verb: PUT
  - description: Delete a Right
    hyperlink: self
    verb: DELETE
  - description: Get a collection of Rights
    hyperlink: self
    verb: GET*
  - description: Get a collection of the Right's Groups
    hyperlink: groups
    verb: GET
  - description: Get a collection of the Right's Roles
    hyperlink: roles
    verb: GET
  - description: Connect the Right to another entity
    hyperlink: connect
    verb: PUT
  - description: Disconnect the Right from another entity
    hyperlink: connect
    verb: DELETE

- name: roles
  description: A Role resource is an arbitrary, named combination of Rights.
  documentation_href: http://wiki.oceanframework.net/The_Role_Resource
  version: v1
  rights:
  - description: Role resource God
  - description: Get a Role
    hyperlink: self
    verb: GET
  - description: Modify a Role
    hyperlink: self
    verb: PUT
  - description: Delete a Role
    hyperlink: self
    verb: DELETE
  - description: Create a Role
    hyperlink: self
    verb: POST
  - description: Get a collection of Roles
    hyperlink: self
    verb: GET*
  - description: Get a collection of the Roles's ApiUsers
    hyperlink: api_users
    verb: GET
  - description: Get a collection of the Roles's Groups
    hyperlink: groups
    verb: GET
  - description: Get a collection of the Roles's Rights
    hyperlink: rights
    verb: GET
  - description: Connect the Role to another entity
    hyperlink: connect
    verb: PUT
  - description: Disconnect the Role from another entity
    hyperlink: connect
    verb: DELETE

- name: groups
  description: A Group resource is an arbitrary, named combination of ApiUsers,
    Roles, and Rights
  documentation_href: http://wiki.oceanframework.net/The_Group_Resource
  version: v1
  rights:
  - description: Group resource God
  - description: Get a Group
    hyperlink: self
    verb: GET
  - description: Modify a Group
    hyperlink: self
    verb: PUT
  - description: Delete a Group
    hyperlink: self
    verb: DELETE
  - description: Create a Group
    hyperlink: self
    verb: POST
  - description: Get a collection of Groups
    hyperlink: self
    verb: GET*
  - description: Get a collection of the Group's ApiUsers
    hyperlink: api_users
    verb: GET
  - description: Get a collection of the Group's Roles
    hyperlink: roles
    verb: GET
  - description: Get a collection of the Group's Rights
    hyperlink: rights
    verb: GET
  - description: Connect the Group to another entity
    hyperlink: connect
    verb: PUT
  - description: Disconnect the Group from another entity
    hyperlink: connect
    verb: DELETE

- name: api_users
  description: An ApiUser is the entity for which an Authentication is made. ApiUsers
    can be real people, but also abstract entities such as Services or clients.
  documentation_href: http://wiki.oceanframework.net/The_ApiUser_Resource
  version: v1
  rights:
  - description: ApiUser resource God
  - description: Get an ApiUser
    hyperlink: self
    verb: GET
  - description: Modify an ApiUser
    hyperlink: self
    verb: PUT
  - description: Delete an ApiUser
    hyperlink: self
    verb: DELETE
  - description: Create an ApiUser
    hyperlink: self
    verb: POST
  - description: Create a Registered ApiUser
    hyperlink: registered
    verb: POST
  - description: Get a collection of ApiUsers
    hyperlink: self
    verb: GET*
  - description: Get a collection of the ApiUser's Authentications
    hyperlink: authentications
    verb: GET
  - description: Get a collection of the ApiUser's Roles
    hyperlink: roles
    verb: GET
  - description: Get a collection of the ApiUser's Groups
    hyperlink: groups
    verb: GET
  - description: Connect the ApiUser to another entity
    hyperlink: connect
    verb: PUT
  - description: Disconnect the ApiUser from another entity
    hyperlink: connect
    verb: DELETE
  - description: Get a collection of the ApiUser's effective Rights
    hyperlink: rights
    verb: GET

- name: authentications
  description: An Authentication resource represents an ApiUser whose identity has
    been verified through its username and its hashed password.
  documentation_href: http://wiki.oceanframework.net/The_Authentication_Resource
  version: v1
  rights:
  - description: Authentication resource God
  - description: Delete an Authentication
    hyperlink: self
    verb: DELETE
  - description: Purge old Authentications
    hyperlink: cleanup
    verb: PUT
