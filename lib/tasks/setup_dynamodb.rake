#
# This task sets up the God Role to include all God Rights.
#

namespace :ocean do
  desc 'Connects to the DynamoDB tables, effectively creating them if not already created'
  task setup_dynamodb: :environment do
    require 'api_user_shadow'
    require 'authentication'

    puts "============================================================"
    puts "Setting up ApiUserShadow..."
    ApiUserShadow.establish_db_connection
    puts "Done."

    puts "Setting up Authentication..."
    Authentication.establish_db_connection
    puts "Done."

    puts "All done."

  end
end
