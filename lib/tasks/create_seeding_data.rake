#
# This task creates the config/seeding_data.yml file out of the contents
# of the config/global folder.
#

namespace :ocean do
  desc 'Creates the config/seeding_data.yml file out of the contents of the config/global folder'
  task create_seeding_data: :environment do
    # Process the file
    f = File.join(Rails.root, 'config/global/api_users.yml')
    api_users = YAML.safe_load(File.read(f))

    # ApiUsers ---------------------------------------------
    users = []
    if ENV['AWS_CONFIG_LOCAL_MODE']
      # If running locally, do nothing: use the default passwords in the file
      api_users.each do |u|
        users << [u['username'], u]
      end
    else
      # But on AWS, retrieve all data from SSM; don't use the file at all
      ssm = Aws::SSM::Client.new(aws_config)
      next_token = nil
      loop do
        passwords = ssm.get_parameters_by_path(
          path: "/#{ENV['VPC_STACK']}/#{OCEAN_ENV}/_passwords",
          recursive: true,
          next_token: next_token
        )
        passwords.parameters.each do |h|
          username = h.name.split('/').last
          users << [username, {
                      "username" => username,
                      "real_name" => "The '#{username}' system user",
                      "password" => h.value,
                      "email" => ENV['SYSTEM_USER_EMAIL_ADDRESS'] || "someone@example.com",
                      "indestructible" => true
                    }
                   ]
        end
        break unless passwords.next_token.present?
        next_token = passwords.next_token
      end
    end

    # Services, Resources, Rights --------------------------
    structure = []
    Dir.foreach(File.join(Rails.root, 'config/global/apps')) do |f|
      next if f == '.' || f == '..'
      f = File.join(Rails.root, 'config/global/apps', f)
      structure << YAML.safe_load(File.read(f))
    end

    # Roles ------------------------------------------------
    roles = []
    Dir.foreach(File.join(Rails.root, 'config/global/roles')) do |f|
      next if f == '.' || f == '..'
      f = File.join(Rails.root, 'config/global/roles', f)
      roles << YAML.safe_load(File.read(f))
    end

    # Groups ------------------------------------------------
    groups = []
    Dir.foreach(File.join(Rails.root, 'config/global/groups')) do |f|
      next if f == '.' || f == '..'
      f = File.join(Rails.root, 'config/global/groups', f)
      groups << YAML.safe_load(File.read(f))
    end

    File.open(File.join(Rails.root, 'config/seeding_data.yml'), 'w') do |f|
      f.puts '#'
      f.puts '# This file is needed to boot the system on AWS.'
      f.puts '# It can be produced by executing:'
      f.puts '#'
      f.puts '#    rake ocean:create_seeding_data'
      f.puts '#'
      f.puts '# This is done automatically when installing the app on the instance.'
      f.puts '#'
      f.write({ 'required_api_users' => users,
                'structure'          => structure,
                'roles'              => roles,
                'groups'             => groups }.to_yaml.gsub(/!ruby\/hash:Mash\s+/, ''))
    end
  end
end
