#
# This task makes sure the DB has the api_users needed for authentication.
# This script will never modify any existing data except password, email and
# real_name. This means that this script can be run at any time exactly in
# order to update those values.
#
# For this reason, this task should be run as part of deployment.
#

namespace :ocean do
  desc 'Updates password, email and real name for all system API users'
  task update_api_users: :environment do
    require 'api_user'

      # This is the generated file, not under source control.
      f = File.join(Rails.root, 'config/seeding_data.yml')

      # If no file to process, abort with an error message
      unless File.exist?(f)
        puts
        puts '-----------------------------------------------------------------------'
        puts 'The generated seeding data file is missing. Please run '
        puts '  rake ocean:create_seeding_data'
        puts 'to create seeding_data.yml from the config/global directory.'
        puts
        puts 'NB: seeding_data.yml is excluded from git version control as it will'
        puts '    contain data private to your Ocean system.'
        puts '-----------------------------------------------------------------------'
        puts
        abort
      end

      puts
      puts '============================================================'
      puts 'Processing ApiUsers...'

      # Process the file
      api_users = YAML.safe_load(File.read(f))['required_api_users']
      puts "The number of ApiUsers to process is #{api_users.length}", ''

      # Attend to each ApiUser
      api_users.each do |username, data|
        user = ApiUser.find_by_username username
        # If the 'delete' flag is set, delete rather than create.
        if data['delete']
          if user
            user.destroy
            puts "Deleted #{data['name']}."
          else
            puts "No need to delete #{data['name']} as it doesn't exist."
          end
          next # Proceed to the next User
        end
        if user
          puts "Updating #{username}."
          user.send(:assign_attributes, data.except('indestructible'))
        else
          puts "Creating #{username}."
          user = ApiUser.new data.merge(username: username).except('indestructible')
        end
        user.indestructible = !!data['indestructible']
        user.save!
      end

      # Set any created_by and updated_by fields which still have the default
      god_id = ApiUser.find_by_username('god').id
      ApiUser.where('created_by = 0 OR updated_by = 0').each do |u|
        if u.created_by == 0
          (begin
             (u.created_by = god_id)
           rescue
             nil
           end)
        end
        if u.created_by == 0
          (begin
             (u.updated_by = god_id)
           rescue
             nil
           end)
        end
        u.save!
      end

      puts 'Done.'
    end
end
