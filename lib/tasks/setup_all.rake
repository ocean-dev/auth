#
# This task creates the config/seeding_data.yml file out of the contents
# of the config/global folder.
#

namespace :ocean do

  desc 'Sets up and/or updates the service data.'
  task setup_all: :environment do
    Rake::Task["ocean:setup_dynamodb"].invoke
    Rake::Task["db:create"].invoke
    Rake::Task["db:migrate"].invoke
    Rake::Task["ocean:create_seeding_data"].invoke
    Rake::Task["ocean:update_api_users"].invoke
    Rake::Task["ocean:update_services_resources_and_rights"].invoke
    Rake::Task["ocean:update_roles"].invoke
    Rake::Task["ocean:update_groups"].invoke
    Rake::Task["ocean:update_god"].invoke
  end
end
