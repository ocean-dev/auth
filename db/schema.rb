# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180920130600) do

  create_table "api_users", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "id",                                      null: false
    t.string   "username",                                null: false
    t.string   "password_hash",                           null: false
    t.string   "password_salt",                           null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "real_name",               default: ""
    t.integer  "lock_version",            default: 0,     null: false
    t.string   "email",                   default: "",    null: false
    t.string   "created_by"
    t.string   "updated_by"
    t.integer  "authentication_duration", default: 10800, null: false
    t.boolean  "login_blocked",           default: false, null: false
    t.string   "login_blocked_reason"
    t.boolean  "indestructible",          default: false, null: false
    t.index ["created_by"], name: "index_api_users_on_created_by", using: :btree
    t.index ["id"], name: "index_api_users_on_id", unique: true, using: :btree
    t.index ["updated_at"], name: "index_api_users_on_updated_at", using: :btree
    t.index ["updated_by"], name: "index_api_users_on_updated_by", using: :btree
    t.index ["username"], name: "index_api_users_on_username", unique: true, using: :btree
  end

  create_table "api_users_groups", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string "api_user_id", null: false
    t.string "group_id",    null: false
    t.index ["api_user_id", "group_id"], name: "index_api_users_groups_on_api_user_id_and_group_id", unique: true, using: :btree
    t.index ["group_id", "api_user_id"], name: "index_api_users_groups_on_group_id_and_api_user_id", unique: true, using: :btree
  end

  create_table "api_users_roles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string "api_user_id", null: false
    t.string "role_id",     null: false
    t.index ["api_user_id", "role_id"], name: "index_api_users_roles_on_api_user_id_and_role_id", unique: true, using: :btree
    t.index ["role_id", "api_user_id"], name: "index_api_users_roles_on_role_id_and_api_user_id", unique: true, using: :btree
  end

  create_table "groups", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "id",                                 null: false
    t.string   "name",                               null: false
    t.string   "description",        default: "",    null: false
    t.integer  "lock_version",       default: 0,     null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "indestructible",     default: false, null: false
    t.string   "documentation_href"
    t.index ["created_by"], name: "index_groups_on_created_by", using: :btree
    t.index ["id"], name: "index_groups_on_id", unique: true, using: :btree
    t.index ["name"], name: "index_groups_on_name", unique: true, using: :btree
    t.index ["updated_at"], name: "index_groups_on_updated_at", using: :btree
    t.index ["updated_by"], name: "index_groups_on_updated_by", using: :btree
  end

  create_table "groups_rights", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string "group_id", null: false
    t.string "right_id", null: false
    t.index ["group_id", "right_id"], name: "index_groups_rights_on_group_id_and_right_id", unique: true, using: :btree
    t.index ["right_id", "group_id"], name: "index_groups_rights_on_right_id_and_group_id", unique: true, using: :btree
  end

  create_table "groups_roles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string "group_id", null: false
    t.string "role_id",  null: false
    t.index ["group_id", "role_id"], name: "index_groups_roles_on_group_id_and_role_id", unique: true, using: :btree
    t.index ["role_id", "group_id"], name: "index_groups_roles_on_role_id_and_group_id", unique: true, using: :btree
  end

  create_table "resources", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "id",                              null: false
    t.string   "name",                            null: false
    t.string   "description",        default: "", null: false
    t.integer  "lock_version",       default: 0,  null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "service_id"
    t.string   "created_by"
    t.string   "updated_by"
    t.string   "documentation_href"
    t.index ["id"], name: "index_resources_on_id", unique: true, using: :btree
    t.index ["name"], name: "index_resources_on_name", unique: true, using: :btree
    t.index ["service_id"], name: "index_resources_on_service_id", using: :btree
    t.index ["updated_at"], name: "index_resources_on_updated_at", using: :btree
  end

  create_table "rights", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "id",                                     null: false
    t.string   "name",                                   null: false
    t.string   "description",              default: "",  null: false
    t.integer  "lock_version",             default: 0,   null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "created_by"
    t.string   "updated_by"
    t.string   "hyperlink",    limit: 128, default: "*", null: false
    t.string   "verb",         limit: 16,  default: "*", null: false
    t.string   "app",          limit: 128, default: "*", null: false
    t.string   "context",      limit: 128, default: "*", null: false
    t.string   "resource_id"
    t.index ["app", "context"], name: "app_rights_index", using: :btree
    t.index ["created_by"], name: "index_rights_on_created_by", using: :btree
    t.index ["id"], name: "index_rights_on_id", unique: true, using: :btree
    t.index ["name"], name: "index_rights_on_name", unique: true, using: :btree
    t.index ["resource_id", "hyperlink", "verb", "app", "context"], name: "main_rights_index", unique: true, using: :btree
    t.index ["updated_at"], name: "index_rights_on_updated_at", using: :btree
    t.index ["updated_by"], name: "index_rights_on_updated_by", using: :btree
  end

  create_table "rights_roles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string "right_id", null: false
    t.string "role_id",  null: false
    t.index ["right_id", "role_id"], name: "index_rights_roles_on_right_id_and_role_id", unique: true, using: :btree
    t.index ["role_id", "right_id"], name: "index_rights_roles_on_role_id_and_right_id", unique: true, using: :btree
  end

  create_table "roles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "id",                                 null: false
    t.string   "name",                               null: false
    t.string   "description",        default: "",    null: false
    t.integer  "lock_version",       default: 0,     null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "indestructible",     default: false, null: false
    t.string   "documentation_href"
    t.index ["created_by"], name: "index_roles_on_created_by", using: :btree
    t.index ["id"], name: "index_roles_on_id", unique: true, using: :btree
    t.index ["name"], name: "index_roles_on_name", unique: true, using: :btree
    t.index ["updated_at"], name: "index_roles_on_updated_at", using: :btree
    t.index ["updated_by"], name: "index_roles_on_updated_by", using: :btree
  end

  create_table "services", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "id",                              null: false
    t.string   "name",                            null: false
    t.string   "description",        default: "", null: false
    t.integer  "lock_version",       default: 0,  null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "created_by"
    t.string   "updated_by"
    t.string   "documentation_href"
    t.index ["created_by"], name: "index_services_on_created_by", using: :btree
    t.index ["id"], name: "index_services_on_id", unique: true, using: :btree
    t.index ["name"], name: "index_services_on_name", unique: true, using: :btree
    t.index ["updated_at"], name: "index_services_on_updated_at", using: :btree
    t.index ["updated_by"], name: "index_services_on_updated_by", using: :btree
  end

end
