class LoadDbSchema < ActiveRecord::Migration[4.2]
  def up
    create_table "api_users", id: false, force: :cascade do |t|
      t.string   "id",                      limit: 255,                 null: false
      t.string   "username",                limit: 255,                 null: false
      t.string   "password_hash",           limit: 255,                 null: false
      t.string   "password_salt",           limit: 255,                 null: false
      t.datetime "created_at",                                          null: false
      t.datetime "updated_at",                                          null: false
      t.string   "real_name",               limit: 255, default: ""
      t.integer  "lock_version",            limit: 4,   default: 0,     null: false
      t.string   "email",                   limit: 255, default: "",    null: false
      t.string   "created_by",              limit: 255
      t.string   "updated_by",              limit: 255
      t.integer  "authentication_duration", limit: 4,   default: 10800, null: false
      t.boolean  "login_blocked",                       default: false, null: false
      t.string   "login_blocked_reason",    limit: 255
      t.boolean  "indestructible",                      default: false, null: false
    end

    add_index "api_users", ["created_by"], name: "index_api_users_on_created_by", using: :btree
    add_index "api_users", ["id"], name: "index_api_users_on_id", unique: true, using: :btree
    add_index "api_users", ["updated_at"], name: "index_api_users_on_updated_at", using: :btree
    add_index "api_users", ["updated_by"], name: "index_api_users_on_updated_by", using: :btree
    add_index "api_users", ["username"], name: "index_api_users_on_username", unique: true, using: :btree

    create_table "api_users_groups", id: false, force: :cascade do |t|
      t.string "api_user_id", limit: 255, null: false
      t.string "group_id",    limit: 255, null: false
    end

    add_index "api_users_groups", ["api_user_id", "group_id"], name: "index_api_users_groups_on_api_user_id_and_group_id", unique: true, using: :btree
    add_index "api_users_groups", ["group_id", "api_user_id"], name: "index_api_users_groups_on_group_id_and_api_user_id", unique: true, using: :btree

    create_table "api_users_roles", id: false, force: :cascade do |t|
      t.string "api_user_id", limit: 255, null: false
      t.string "role_id",     limit: 255, null: false
    end

    add_index "api_users_roles", ["api_user_id", "role_id"], name: "index_api_users_roles_on_api_user_id_and_role_id", unique: true, using: :btree
    add_index "api_users_roles", ["role_id", "api_user_id"], name: "index_api_users_roles_on_role_id_and_api_user_id", unique: true, using: :btree

    create_table "groups", id: false, force: :cascade do |t|
      t.string   "id",                 limit: 255,                 null: false
      t.string   "name",               limit: 255,                 null: false
      t.string   "description",        limit: 255, default: "",    null: false
      t.integer  "lock_version",       limit: 4,   default: 0,     null: false
      t.datetime "created_at",                                     null: false
      t.datetime "updated_at",                                     null: false
      t.string   "created_by",         limit: 255
      t.string   "updated_by",         limit: 255
      t.boolean  "indestructible",                 default: false, null: false
      t.string   "documentation_href", limit: 255
    end

    add_index "groups", ["created_by"], name: "index_groups_on_created_by", using: :btree
    add_index "groups", ["id"], name: "index_groups_on_id", unique: true, using: :btree
    add_index "groups", ["name"], name: "index_groups_on_name", unique: true, using: :btree
    add_index "groups", ["updated_at"], name: "index_groups_on_updated_at", using: :btree
    add_index "groups", ["updated_by"], name: "index_groups_on_updated_by", using: :btree

    create_table "groups_rights", id: false, force: :cascade do |t|
      t.string "group_id", limit: 255, null: false
      t.string "right_id", limit: 255, null: false
    end

    add_index "groups_rights", ["group_id", "right_id"], name: "index_groups_rights_on_group_id_and_right_id", unique: true, using: :btree
    add_index "groups_rights", ["right_id", "group_id"], name: "index_groups_rights_on_right_id_and_group_id", unique: true, using: :btree

    create_table "groups_roles", id: false, force: :cascade do |t|
      t.string "group_id", limit: 255, null: false
      t.string "role_id",  limit: 255, null: false
    end

    add_index "groups_roles", ["group_id", "role_id"], name: "index_groups_roles_on_group_id_and_role_id", unique: true, using: :btree
    add_index "groups_roles", ["role_id", "group_id"], name: "index_groups_roles_on_role_id_and_group_id", unique: true, using: :btree

    create_table "resources", id: false, force: :cascade do |t|
      t.string   "id",                 limit: 255,              null: false
      t.string   "name",               limit: 255,              null: false
      t.string   "description",        limit: 255, default: "", null: false
      t.integer  "lock_version",       limit: 4,   default: 0,  null: false
      t.datetime "created_at",                                  null: false
      t.datetime "updated_at",                                  null: false
      t.string   "service_id",         limit: 255
      t.string   "created_by",         limit: 255
      t.string   "updated_by",         limit: 255
      t.string   "documentation_href", limit: 255
    end

    add_index "resources", ["id"], name: "index_resources_on_id", unique: true, using: :btree
    add_index "resources", ["name"], name: "index_resources_on_name", unique: true, using: :btree
    add_index "resources", ["service_id"], name: "index_resources_on_service_id", using: :btree
    add_index "resources", ["updated_at"], name: "index_resources_on_updated_at", using: :btree

    create_table "rights", id: false, force: :cascade do |t|
      t.string   "id",           limit: 255,               null: false
      t.string   "name",         limit: 255,               null: false
      t.string   "description",  limit: 255, default: "",  null: false
      t.integer  "lock_version", limit: 4,   default: 0,   null: false
      t.datetime "created_at",                             null: false
      t.datetime "updated_at",                             null: false
      t.string   "created_by",   limit: 255
      t.string   "updated_by",   limit: 255
      t.string   "hyperlink",    limit: 128, default: "*", null: false
      t.string   "verb",         limit: 16,  default: "*", null: false
      t.string   "app",          limit: 128, default: "*", null: false
      t.string   "context",      limit: 128, default: "*", null: false
      t.string   "resource_id",  limit: 255
    end

    add_index "rights", ["app", "context"], name: "app_rights_index", using: :btree
    add_index "rights", ["created_by"], name: "index_rights_on_created_by", using: :btree
    add_index "rights", ["id"], name: "index_rights_on_id", unique: true, using: :btree
    add_index "rights", ["name"], name: "index_rights_on_name", unique: true, using: :btree
    add_index "rights", ["resource_id", "hyperlink", "verb", "app", "context"], name: "main_rights_index", unique: true, using: :btree
    add_index "rights", ["updated_at"], name: "index_rights_on_updated_at", using: :btree
    add_index "rights", ["updated_by"], name: "index_rights_on_updated_by", using: :btree

    create_table "rights_roles", id: false, force: :cascade do |t|
      t.string "right_id", limit: 255, null: false
      t.string "role_id",  limit: 255, null: false
    end

    add_index "rights_roles", ["right_id", "role_id"], name: "index_rights_roles_on_right_id_and_role_id", unique: true, using: :btree
    add_index "rights_roles", ["role_id", "right_id"], name: "index_rights_roles_on_role_id_and_right_id", unique: true, using: :btree

    create_table "roles", id: false, force: :cascade do |t|
      t.string   "id",                 limit: 255,                 null: false
      t.string   "name",               limit: 255,                 null: false
      t.string   "description",        limit: 255, default: "",    null: false
      t.integer  "lock_version",       limit: 4,   default: 0,     null: false
      t.datetime "created_at",                                     null: false
      t.datetime "updated_at",                                     null: false
      t.string   "created_by",         limit: 255
      t.string   "updated_by",         limit: 255
      t.boolean  "indestructible",                 default: false, null: false
      t.string   "documentation_href", limit: 255
    end

    add_index "roles", ["created_by"], name: "index_roles_on_created_by", using: :btree
    add_index "roles", ["id"], name: "index_roles_on_id", unique: true, using: :btree
    add_index "roles", ["name"], name: "index_roles_on_name", unique: true, using: :btree
    add_index "roles", ["updated_at"], name: "index_roles_on_updated_at", using: :btree
    add_index "roles", ["updated_by"], name: "index_roles_on_updated_by", using: :btree

    create_table "services", id: false, force: :cascade do |t|
      t.string   "id",                 limit: 255,              null: false
      t.string   "name",               limit: 255,              null: false
      t.string   "description",        limit: 255, default: "", null: false
      t.integer  "lock_version",       limit: 4,   default: 0,  null: false
      t.datetime "created_at",                                  null: false
      t.datetime "updated_at",                                  null: false
      t.string   "created_by",         limit: 255
      t.string   "updated_by",         limit: 255
      t.string   "documentation_href", limit: 255
    end

    add_index "services", ["created_by"], name: "index_services_on_created_by", using: :btree
    add_index "services", ["id"], name: "index_services_on_id", unique: true, using: :btree
    add_index "services", ["name"], name: "index_services_on_name", unique: true, using: :btree
    add_index "services", ["updated_at"], name: "index_services_on_updated_at", using: :btree
    add_index "services", ["updated_by"], name: "index_services_on_updated_by", using: :btree
  end

  def down
    drop_table 'api_users'
    drop_table 'api_users_groups'
    drop_table 'api_users_roles'
    drop_table 'groups'
    drop_table 'groups_rights'
    drop_table 'groups_roles'
    drop_table 'resources'
    drop_table 'rights'
    drop_table 'rights_roles'
    drop_table 'roles'
    drop_table 'services'
  end
end
