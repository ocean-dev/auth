require 'simplecov'
SimpleCov.start do
  add_filter '/vendor/'
  add_filter '/spec/support/hyperlinks.rb'
end

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

# # DynamoDB table cleaner
# OCEAN_ENV = "master" unless defined?(OCEAN_ENV)
# regexp = Regexp.new("^.+_#{OCEAN_ENV}_[0-9]{1,3}-[0-9]{1,3}-[0-9]{1,3}-[0-9]{1,3}_(test|dev)$")
# cleaner = lambda {
#   c = Aws::DynamoDB::Client.new
#   c.list_tables.table_names.each do |t|
#     begin
#       c.delete_table({table_name: t}) if t =~ regexp
#     rescue Aws::DynamoDB::Errors::LimitExceededException
#       sleep 1
#       retry
#     end
#   end
# }

RSpec.configure do |config|
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  # config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = 'random'

  # Make "FactoryBot" superfluous
  config.include FactoryBot::Syntax::Methods

  # RSpec 3 compatibility
  config.infer_spec_type_from_file_location!

  # config.before(:suite) { cleaner.call }
  # config.after(:suite) { cleaner.call }

  config.example_status_persistence_file_path = "tmp/rspec_examples.txt"
  config.run_all_when_everything_filtered = true

end
