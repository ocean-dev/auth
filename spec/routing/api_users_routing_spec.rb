require 'spec_helper'

describe ApiUsersController do
  describe 'routing' do
    it 'routes to #index' do
      expect(get('/v1/api_users')).to route_to('api_users#index')
    end

    it 'routes to #show' do
      expect(get('/v1/api_users/a-b-c-d-e')).to route_to('api_users#show', id: 'a-b-c-d-e')
    end

    it 'routes to #create' do
      expect(post('/v1/api_users')).to route_to('api_users#create')
    end

    it 'routes to #update' do
      expect(put('/v1/api_users/a-b-c-d-e')).to route_to('api_users#update', id: 'a-b-c-d-e')
    end

    it 'routes to #destroy' do
      expect(delete('/v1/api_users/a-b-c-d-e')).to route_to('api_users#destroy', id: 'a-b-c-d-e')
    end

    it 'routes to #authentications' do
      expect(get('/v1/api_users/a-b-c-d-e/authentications')).to route_to('api_users#authentications', id: 'a-b-c-d-e')
    end

    it 'routes to #roles' do
      expect(get('/v1/api_users/a-b-c-d-e/roles')).to route_to('api_users#roles', id: 'a-b-c-d-e')
    end

    it 'routes to #groups' do
      expect(get('/v1/api_users/a-b-c-d-e/groups')).to route_to('api_users#groups', id: 'a-b-c-d-e')
    end

    it 'routes to #rights' do
      expect(get('/v1/api_users/a-b-c-d-e/rights')).to route_to('api_users#rights', id: 'a-b-c-d-e')
    end

    it 'routes to #connect to connect to another resource' do
      expect(put('/v1/api_users/a-b-c-d-e/connect')).to route_to('api_users#connect', id: 'a-b-c-d-e')
    end

    it 'routes to #disconnect to disconnect from a connected resource' do
      expect(delete('/v1/api_users/a-b-c-d-e/connect')).to route_to('api_users#disconnect', id: 'a-b-c-d-e')
    end
  end
end
