require 'spec_helper'

describe RolesController do
  describe 'routing' do
    it 'routes to #index' do
      expect(get('/v1/roles')).to route_to('roles#index')
    end

    it 'routes to #show' do
      expect(get('/v1/roles/a-b-c-d-e')).to route_to('roles#show', id: 'a-b-c-d-e')
    end

    it 'routes to #create' do
      expect(post('/v1/roles')).to route_to('roles#create')
    end

    it 'routes to #update' do
      expect(put('/v1/roles/a-b-c-d-e')).to route_to('roles#update', id: 'a-b-c-d-e')
    end

    it 'routes to #destroy' do
      expect(delete('/v1/roles/a-b-c-d-e')).to route_to('roles#destroy', id: 'a-b-c-d-e')
    end

    it 'routes to #api_users' do
      expect(get('/v1/roles/a-b-c-d-e/api_users')).to route_to('roles#api_users', id: 'a-b-c-d-e')
    end

    it 'routes to #groups' do
      expect(get('/v1/roles/a-b-c-d-e/groups')).to route_to('roles#groups', id: 'a-b-c-d-e')
    end

    it 'routes to #rights' do
      expect(get('/v1/roles/a-b-c-d-e/rights')).to route_to('roles#rights', id: 'a-b-c-d-e')
    end

    it 'routes to #connect to connect to another resource' do
      expect(put('/v1/roles/a-b-c-d-e/connect')).to route_to('roles#connect', id: 'a-b-c-d-e')
    end

    it 'routes to #disconnect to disconnect from a connected resource' do
      expect(delete('/v1/roles/a-b-c-d-e/connect')).to route_to('roles#disconnect', id: 'a-b-c-d-e')
    end
  end
end
