require 'spec_helper'

describe RightsController do
  describe 'routing' do
    it 'routes to #index' do
      expect(get('/v1/rights')).to route_to('rights#index')
    end

    it 'routes to #show' do
      expect(get('/v1/rights/a-b-c-d-e')).to route_to('rights#show', id: 'a-b-c-d-e')
    end

    it 'not route to #create' do
      expect(post('/v1/rights')).not_to be_routable
    end

    it 'routes to #update' do
      expect(put('/v1/rights/a-b-c-d-e')).to route_to('rights#update', id: 'a-b-c-d-e')
    end

    it 'routes to #destroy' do
      expect(delete('/v1/rights/a-b-c-d-e')).to route_to('rights#destroy', id: 'a-b-c-d-e')
    end

    it 'routes to #groups' do
      expect(get('/v1/rights/a-b-c-d-e/groups')).to route_to('rights#groups', id: 'a-b-c-d-e')
    end

    it 'routes to #roles' do
      expect(get('/v1/rights/a-b-c-d-e/roles')).to route_to('rights#roles', id: 'a-b-c-d-e')
    end

    it 'routes to #connect to connect to another resource' do
      expect(put('/v1/rights/a-b-c-d-e/connect')).to route_to('rights#connect', id: 'a-b-c-d-e')
    end

    it 'routes to #disconnect to disconnect from a connected resource' do
      expect(delete('/v1/rights/a-b-c-d-e/connect')).to route_to('rights#disconnect', id: 'a-b-c-d-e')
    end
  end
end
