require 'spec_helper'

describe GroupsController do
  describe 'routing' do
    it 'routes to #index' do
      expect(get('/v1/groups')).to route_to('groups#index')
    end

    it 'routes to #show' do
      expect(get('/v1/groups/a-b-c-d-e')).to route_to('groups#show', id: 'a-b-c-d-e')
    end

    it 'routes to #create' do
      expect(post('/v1/groups')).to route_to('groups#create')
    end

    it 'routes to #update' do
      expect(put('/v1/groups/a-b-c-d-e')).to route_to('groups#update', id: 'a-b-c-d-e')
    end

    it 'routes to #destroy' do
      expect(delete('/v1/groups/a-b-c-d-e')).to route_to('groups#destroy', id: 'a-b-c-d-e')
    end

    it 'routes to #api_users' do
      expect(get('/v1/groups/a-b-c-d-e/api_users')).to route_to('groups#api_users', id: 'a-b-c-d-e')
    end

    it 'routes to #roles' do
      expect(get('/v1/groups/a-b-c-d-e/roles')).to route_to('groups#roles', id: 'a-b-c-d-e')
    end

    it 'routes to #rights' do
      expect(get('/v1/groups/a-b-c-d-e/rights')).to route_to('groups#rights', id: 'a-b-c-d-e')
    end

    it 'routes to #connect to connect to another resource' do
      expect(put('/v1/groups/a-b-c-d-e/connect')).to route_to('groups#connect', id: 'a-b-c-d-e')
    end

    it 'routes to #disconnect to disconnect from a connected resource' do
      expect(delete('/v1/groups/a-b-c-d-e/connect')).to route_to('groups#disconnect', id: 'a-b-c-d-e')
    end
  end
end
