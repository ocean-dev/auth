require 'spec_helper'

describe AuthenticationsController do
  render_views

  describe 'POST' do
    before :each do
      Authentication.destroy_all
      ApiUser.destroy_all
      request.headers['HTTP_ACCEPT'] = 'application/json'
    end

    it 'should return JSON' do
      post :create
      expect(response.content_type).to eq('application/json')
    end

    it 'should not require an X-API-Token header' do
      create :api_user, username: 'myuser', password: 'mypassword'
      request.headers['X-API-Authenticate'] = ::Base64.strict_encode64('myuser:mypassword')
      request.headers['X-API-Token'] = nil
      post :create
      expect(response.content_type).to eq('application/json')
      expect(response.status).to eq(201)
    end

    it 'must return 400 if no X-API-Authenticate header was provided' do
      post :create
      expect(response.content_type).to eq('application/json')
      expect(response.status).to eq(400)
      expect(JSON.parse(response.body)).to eq('_api_error' => ['Malformed credentials'])
    end

    it 'must return 403 if the X-API-Authenticate user is unknown' do
      request.headers['X-API-Authenticate'] = ::Base64.strict_encode64('nonexistentuser:somepassword')
      post :create
      expect(response.content_type).to eq('application/json')
      expect(response.status).to eq(403)
      expect(JSON.parse(response.body)).to eq('_api_error' => ['Does not authenticate'])
    end

    it "must return 403 if the X-API-Authenticate credentials don't match" do
      create :api_user, username: 'myuser', password: 'mypassword'
      request.headers['X-API-Authenticate'] = ::Base64.strict_encode64('myuser:wrong')
      post :create
      expect(response.content_type).to eq('application/json')
      expect(response.status).to eq(403)
      expect(JSON.parse(response.body)).to eq('_api_error' => ['Does not authenticate'])
    end

    it 'must return 403 with a reason if the ApiUser is blocked' do
      create :api_user, username: 'myuser', password: 'mypassword',
                        login_blocked: true, login_blocked_reason: 'So there!'
      request.headers['X-API-Authenticate'] = ::Base64.strict_encode64('myuser:mypassword')
      post :create
      expect(response.content_type).to eq('application/json')
      expect(response.status).to eq(403)
      expect(JSON.parse(response.body)).to eq('_api_error' => ['Login blocked', 'So there!'])
    end

    it 'must return a 201 if the X-API-Authenticate credentials match' do
      create :api_user, username: 'myuser', password: 'mypassword'
      request.headers['X-API-Authenticate'] = ::Base64.strict_encode64('myuser:mypassword')
      post :create
      expect(response.content_type).to eq('application/json')
      expect(response.status).to eq(201)
    end

    it 'should return a complete resource when successful' do
      create :api_user, username: 'myuser', password: 'mypassword'
      request.headers['X-API-Authenticate'] = ::Base64.strict_encode64('myuser:mypassword')
      post :create
      expect(response).to render_template(partial: '_authentication', count: 1)
      expect(response.content_type).to eq('application/json')
      expect(response.status).to eq(201)
      expect(response.headers['Location']).to be_blank
      r = JSON.parse(response.body)
      expect(r).to be_a Hash # The structural tests are done in the view specs
    end

    it 'should return an Authentication with its max_age set to the authentication_duration of its ApiUser' do
      t = 1.year.to_i
      @api_user = create :api_user, username: 'myuser', password: 'mypassword', authentication_duration: t
      request.headers['X-API-Authenticate'] = ::Base64.strict_encode64('myuser:mypassword')
      request.headers['X-API-Token'] = nil
      post :create
      expect(response.content_type).to eq('application/json')
      expect(response.status).to eq(201)
      max_age = JSON.parse(response.body)['authentication']['max_age']
      expect(max_age).to eq(@api_user.authentication_duration)
      expect(max_age).to eq(t)
    end

    it 'should not create another Authentication if a valid one already exists' do
      create :api_user, username: 'myuser', password: 'mypassword'
      request.headers['X-API-Authenticate'] = ::Base64.strict_encode64('myuser:mypassword')
      post :create
      expect(Authentication.all.count).to eq(1)
      sleep 1
      request.headers['X-API-Authenticate'] = ::Base64.strict_encode64('myuser:mypassword')
      post :create
      sleep 1
      expect(Authentication.all.count).to eq(1)
    end
  end
end
