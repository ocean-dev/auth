require 'spec_helper'

describe RolesController do
  render_views

  describe 'GET' do
    before :each do
      permit_with 200
      @role = create :role
      request.headers['HTTP_ACCEPT'] = 'application/json'
      request.headers['X-API-Token'] = 'totally-fake'
    end

    it 'should return JSON' do
      get :show, params: { id: @role }
      expect(response.content_type).to eq('application/json')
    end

    it 'should return a 400 if the X-API-Token header is missing' do
      request.headers['X-API-Token'] = nil
      get :show, params: { id: @role }
      expect(response.status).to eq(400)
      expect(response.content_type).to eq('application/json')
    end

    it "should return a 404 when the user can't be found" do
      get :show, params: { id: '0-0-0-0-0' }
      expect(response.status).to eq(404)
      expect(response.content_type).to eq('application/json')
    end

    it 'should return a 200 when successful' do
      get :show, params: { id: @role }
      expect(response).to render_template(partial: '_role', count: 1)
      expect(response.status).to eq(200)
    end
  end
end
