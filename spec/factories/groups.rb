# == Schema Information
#
# Table name: groups
#
#  id                 :string           primary key
#  name               :string           not null
#  description        :string           default(""), not null
#  lock_version       :integer          default(0), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  created_by         :string
#  updated_by         :string
#  indestructible     :boolean          default(FALSE), not null
#  documentation_href :string
#
# Indexes
#
#  index_groups_on_created_by  (created_by)
#  index_groups_on_id          (id) UNIQUE
#  index_groups_on_name        (name) UNIQUE
#  index_groups_on_updated_at  (updated_at)
#  index_groups_on_updated_by  (updated_by)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :group do
    name         { "group_#{rand(1_000_000)}" }
    description  { 'This is a description of the Group.' }
    lock_version { 0 }
    created_by  { 'https://api.example.com/v1/api_users/a-b-c-d-e' }
    updated_by  { 'https://api.example.com/v1/api_users/a-b-c-d-e' }
  end
end
