# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :resource do
    name         { "resource_#{rand(1_000_000)}" }
    description  { 'This is a description of the Resource.' }
    lock_version { 0 }
    service
    created_by  { 'https://api.example.com/v1/api_users/a-b-c-d-e' }
    updated_by  { 'https://api.example.com/v1/api_users/a-b-c-d-e' }
  end
end
