# == Schema Information
#
# Table name: api_users
#
#  id                      :string           primary key
#  username                :string           not null
#  password_hash           :string           not null
#  password_salt           :string           not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  real_name               :string           default("")
#  lock_version            :integer          default(0), not null
#  email                   :string           default(""), not null
#  created_by              :string
#  updated_by              :string
#  authentication_duration :integer          default(10800), not null
#  login_blocked           :boolean          default(FALSE), not null
#  login_blocked_reason    :string
#  indestructible          :boolean          default(FALSE), not null
#
# Indexes
#
#  index_api_users_on_created_by  (created_by)
#  index_api_users_on_id          (id) UNIQUE
#  index_api_users_on_updated_at  (updated_at)
#  index_api_users_on_updated_by  (updated_by)
#  index_api_users_on_username    (username) UNIQUE
#

FactoryBot.define do
  factory :api_user do
    username	{ "user_#{rand(100_000_000)}" }
    password	{ "password_#{rand(10_000_000)}" }
    email       { "somebody_#{rand(10_000_000)}@example.com" }
    created_by  { 'https://api.example.com/v1/api_users/a-b-c-d-e' }
    updated_by  { 'https://api.example.com/v1/api_users/a-b-c-d-e' }
  end
end
