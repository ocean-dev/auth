# == Schema Information
#
# Table name: rights
#
#  id           :string           primary key
#  name         :string           not null
#  description  :string           default(""), not null
#  lock_version :integer          default(0), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  created_by   :string
#  updated_by   :string
#  hyperlink    :string(128)      default("*"), not null
#  verb         :string(16)       default("*"), not null
#  app          :string(128)      default("*"), not null
#  context      :string(128)      default("*"), not null
#  resource_id  :string
#
# Indexes
#
#  app_rights_index            (app,context)
#  index_rights_on_created_by  (created_by)
#  index_rights_on_id          (id) UNIQUE
#  index_rights_on_name        (name) UNIQUE
#  index_rights_on_updated_at  (updated_at)
#  index_rights_on_updated_by  (updated_by)
#  main_rights_index           (resource_id,hyperlink,verb,app,context) UNIQUE
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :right do
    description  { 'This is a description of the Right.' }
    lock_version { 0 }
    resource
    hyperlink    { "hyperlink_#{rand(1_000_000)}" }
    verb         { '*' }
    app          { "app_#{rand(1_000_000)}" }
    context      { "context_#{rand(1_000_000)}" }
    created_by  { 'https://api.example.com/v1/api_users/a-b-c-d-e' }
    updated_by  { 'https://api.example.com/v1/api_users/a-b-c-d-e' }
  end
end
